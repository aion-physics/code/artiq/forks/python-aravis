from setuptools import setup


setup (name = "aravis", 
        version = "0.5.1",
        description = "pythonic interface to auto-generated aravis python bindings",
        author = "Olivier Roulet-Dubonnet",
        url = 'https://github.com/oroulet/python-aravis',
        py_modules=["aravis"],
        license = "GNU General Public License",
        install_requires = [
                "numpy",
                "PyGObject"
        ]
)
